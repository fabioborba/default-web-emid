;(function ($, document, window, undefined){


	/*
	 * Banner main
	 */
    $( '#slider' ).find( '.slick-slider' ).slick({
        adaptiveHeight: true,
        slide: 'li',
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });










    /*
     * Slider - single slider
     */
    var singleSlider = $( '.single-slider' );

    if ( singleSlider.length ){

        singleSlider.slick({
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true
        });

    };











	/*
     * Request All Form
     */
    var formAlert = '.formee-msg',
    	status      = {
            'success'   : 'success',
            'error'     : 'error',
            'warning'   : 'warning',
            'info'      : 'info'
        };


    var form = (function (){


        var formPost = function (target, serialize){

            $.ajax({
                url     : target.attr( 'action' ) || 'action.php',
                type    : target.attr( 'method' ) || 'POST',
                dataType: 'json',
                data    : serialize || target.serialize(),
            })
            .done(function (res, textStatus, jqXHR){
                setTimeout(function(){

                    target
                    	.trigger('submitSuccess', [res, textStatus, jqXHR])
                    	.trigger('showAlert', [res.status || 'success', res.message, 5500]);

                }, 1200);
            })
            .fail(function (jqXHR, textStatus, errorThrown){
                target.trigger('submitFail', [jqXHR, textStatus, errorThrown]);
                target.trigger('showAlert', ['error', '<b>ERRO</b> ao conectar ao servidor!', 4500]);
            })
            .always(function (){
                setTimeout(function (){
                    target.find( 'input[type="submit"], button' ).prop('disabled', false);
                }, 1300);
            });

        },

        clearAlert = function (target){
            target.removeClass('success error warning info show');
        },

        showAlert = function (target, status, message){

            var obj = target.find( formAlert );

            if ( !obj.length ) return false;
            clearAlert( obj );
            obj.addClass('show ' + status).find( '.message' ).html( message );

        },

        basicValidate = function (event){

            var target = $(event.target);
            if ( checkFields(target) ) event.data.success(target);
            else event.data.erro(target);
            return false;

        };


        return {

            post: formPost,
            notify: showAlert,
            basicValidate: basicValidate,
            clearAlert: clearAlert

        };


    } ());


    /* All event form */
    $( 'form' ).on('submitHandler', function (event){
        
        form.post( $(event.target) );
        return false;

    }).on('showAlert', function (event, state, message, time){
        
        form.notify($( event.target ), status[state], message);
        if ( time ){
            clearTimeout( timer );
            var timer = setTimeout(function (){
                form.clearAlert( $( event.target ).find(formAlert) );
            }, time);
        };

    }).on('change', 'input[type="file"]', function (event){

        console.log( this.value );
        var el = $(this).parent();
        el.find( '.filename' ).text( this.value );

    });


    /*
     * Close alert
     */
    $( formAlert ).on('click', '.closed', function (event){

        $( formAlert ).slideUp(150, function (){
            form.clearAlert( $(this) );
            $(this).removeAttr('style');
        });
        return false;

    });


} (jQuery, document, window));