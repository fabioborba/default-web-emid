<?php
	
	require_once '../bootstrap/bootstrap.php';
	$page = array(
		'title' => 'Entre em contato'
	);

    /* Breadcrumb */
    $breadcrumb = array(
        $page['title']
    );

?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<?php include $_path['includes'] . 'head.php'; ?>
</head>
<body>

	<?php include $_path['includes'] . 'header.php'; ?>

	<main class="main wrapper">
	
		<header class="headline-title">
			<?php include $_path['includes'] . 'breadcrumb.php'; ?>
			<h1 class="title"><?php echo $page['title']; ?></h1>
		</header><!-- .headline-title -->


		<section class="main-container">
			
			<div class="main-content">
				<form action="form.php" class="formee validate">
                    
                    <h4>Escreva sua dúvida, crítica ou sugestão diretamente para a Coordenação <?php echo $_config['company']; ?>.</h4>

                    <fieldset class="row">
                        
                        <div class="field col s12 m12 l8">
                            <input type="text" id="name" name="name" placeholder="Nome completo:" required title="Insira seu nome">
                        </div>
                        <div class="field col s12 m6 l4">
                            <input type="tel" id="phone" name="phone" placeholder="(xx) XXXX-XXXX" required title="Seu telefone">
                        </div>

                        <div class="field col s12 m6 l12">
                            <input type="text" id="email" name="email" placeholder="seuemail@email.com" required title="Insira seu e-mail">
                        </div>

                        <div class="field col s12 m6 l12">
                            <input type="text" id="assunto" name="assunto" placeholder="Assunto" required title="Insira um assunto">
                        </div>

                        <div class="field col s12">
                            <textarea name="message" id="" rows="3" placeholder="Insira sua mensagem:"></textarea>
                        </div>

                        <div class="field col s12 l6 offset-l6">
                            <input type="submit" class="btn-default btn-full" value="ENVIAR">
                        </div>

                        <input type="hidden" name="action" value="contato">

                    </fieldset>

                    <div class="formee-msg alert">
                        <p class="message"><strong>Por favor,</strong> preencha todos os campos!</p>
                        <button class="closed">×</button>
                    </div>

                </form>

			</div><!-- .main-content -->

		</section><!-- .main-container -->

	</main><!-- .main -->


	<?php include $_path['includes'] . 'footer.php'; ?>

	<script ype="text/javascript">
    ;(function() {
        
        var script,
            scripts = document.getElementsByTagName('script')[0];
            
        function load(url) {
          script = document.createElement('script');
          script.async = true;
          script.src = url;
          scripts.parentNode.insertBefore(script, scripts);
        }
        
        load('js/vendor/jquery.validate.min.js');
        load('js/validate.min.js');
        
    }());
    </script>

	
</body>
</html>