<?php
	
	require_once '../bootstrap/bootstrap.php';

?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<?php include $_path['includes'] . 'head.php';
	echo getHeadersIndex();
	 ?>
</head>
<body class="pg-home">

	<?php include $_path['includes'] . 'header.php'; ?>

	
	<figure id="slider">
		<ul class="slick-slider">
			<li><img src="img/tmp/slide1.jpg" alt=""></li>
			<li><img src="img/tmp/slide2.jpg" alt=""></li>
			<li><img src="img/tmp/slide3.jpg" alt=""></li>
		</ul>
	</figure>


	<main class="main wrapper">
	
		

	</main><!-- .main -->

	
	
	<?php include $_path['includes'] . 'footer.php'; ?>
	
	
	<script ype="text/javascript">
    ;(function() {
        
        var script,
            scripts = document.getElementsByTagName('script')[0];
            
        function load(url) {
          script = document.createElement('script');
          script.async = true;
          script.src = url;
          scripts.parentNode.insertBefore(script, scripts);
        }
        
        load('js/vendor/jquery.validate.min.js');
        load('js/validate.min.js');
        
    }());
    </script>


	<div id="fb-root"></div>
	<script async="async">(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
</body>
</html>