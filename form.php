<?php

require_once "bootstrap/bootstrap.php";

$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : null;

switch ($action)
{
    case "contato":
        formContato();
    break;

    default:
        exit;
    break;
}

function formContato()
{

    global $_container, $_config;

    $name       = isset($_REQUEST["name"]) ? trim($_REQUEST["name"]) : null;
    $email      = isset($_REQUEST["email"]) && filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL) ? trim($_REQUEST["email"]) : null;
    $phone      = isset($_REQUEST["phone"]) ? trim($_REQUEST["phone"]) : null;
    $assunto    = isset($_REQUEST["assunto"]) ? trim($_REQUEST["assunto"]) : null;
    $message    = isset($_REQUEST["message"]) ? trim($_REQUEST["message"]) : null;

    $contato = $_container['contato'];

    $contato->setNome($name);
    $contato->setEmail($email);
    $contato->setTelefone($phone);
    $contato->setAssunto($assunto);
    $contato->setMensagem($message);

    $_container['contatos']->save($contato);

    /**
     * Send mail
     */
    $mailer = $_container['mail'];

    $text = '<b>CONTATO PELO SITE ' . mb_strtoupper($_config['company'], 'UTF-8') . '</b><br /><br />
            Hora/Data: ' . date('H:i \d\o \d\i\a d/m/Y') . '<br />
            Nome: ' . $name . '<br />
            E-mail: ' . $email . '<br />
            Telefone: ' . $phone . '<br />
            Assunto: ' . $assunto . '<br />
            IP: ' . $_SERVER['REMOTE_ADDR'] . '<br />
            Mensagem: ' . nl2br($message);

    $message = Swift_Message::newInstance($assunto)
      ->setFrom(array($email => $name))
      ->setTo(array($_config['mail'] => $_config['company']))
      ->setBody($text, 'text/html');

    $result = $mailer->send($message);

    if( is_ajax() ){
        $return['status']   = 'success'; // accept custom style ['success', 'error', 'warning', 'info']
        $return['message']  = '<b>E-mail enviado com sucesso!</b>';

        echo json_encode( $return );
    }

}

