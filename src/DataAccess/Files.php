<?php

namespace DataAccess;

use PDO;
use DataAccess\Entity\File;

class Files
{
	private $pdo;

	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function all($type, $module, $relashionship)
	{
		$stmt = $this->pdo->prepare('select * from tbl_files where type = :type and module = :module and relashionship = :relashionship');
		$stmt->bindValue(':type', $type, PDO::PARAM_STR);
		$stmt->bindValue(':module', $module, PDO::PARAM_STR);
		$stmt->bindValue(':relashionship', $relashionship, PDO::PARAM_INT);
		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $data;
	}

	public function find($id)
	{
		$stmt = $this->pdo->prepare('select * from tbl_files where id = :id');
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		return $data;
	}

	public function first($type, $module, $relashionship)
	{
		$all = $this->all($type, $module, $relashionship);

		if( count($all) > 0 ){
			return $all[0];
		}

		return false;
	}

	public function save(File $file)
	{
		$stmt = $this->pdo->prepare('insert into tbl_files (filename, title, type, module, relashionship, created_at) values (:filename, :title, :type, :module, :relashionship, :created_at)');
        $stmt->bindValue(':filename', $file->getFilename(), PDO::PARAM_STR);
        $stmt->bindValue(':title', $file->getTitle(), PDO::PARAM_STR);
        $stmt->bindValue(':type', $file->getType(), PDO::PARAM_STR);
        $stmt->bindValue(':module', $file->getModule(), PDO::PARAM_STR);
        $stmt->bindValue(':relashionship', $file->getRelashionship(), PDO::PARAM_INT);
        $stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
        $stmt->execute();
	}

	public function destroy(File $file)
	{
		$path  = sprintf('%s/%s/%d', $file->getPath(), $file->getModule(), $file->getRelashionship());
		$thumb = sprintf('%s/thumb_%s', $path, $file->getFilename());
		$large = sprintf('%s/large_%s', $path, $file->getFilename());

		if( is_file($thumb) ){
			unlink($thumb);
		}

		if( is_file($large) ){
			unlink($large);
		}

		$stmt = $this->pdo->prepare('delete from tbl_files where id = :id');
		$stmt->bindValue(':id', $file->getId(), PDO::PARAM_INT);
        $stmt->execute();
	}

	public function destroyAll(File $file)
	{
		$stmt = $this->pdo->prepare('select id, filename from tbl_files where type = :type and module = :module and relashionship = :relashionship');
		$stmt->bindValue(':type', $file->getType(), PDO::PARAM_STR);
        $stmt->bindValue(':module', $file->getModule(), PDO::PARAM_STR);
        $stmt->bindValue(':relashionship', $file->getRelashionship(), PDO::PARAM_INT);
        $stmt->execute();

        $files = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($files as $f){
			$path  = sprintf('%s/%s/%d', $file->getPath(), $file->getModule(), $file->getRelashionship());
			$thumb = sprintf('%s/thumb_%s', $path, $f['filename']);
			$large = sprintf('%s/large_%s', $path, $f['filename']);

			if( is_file($thumb) ){
				unlink($thumb);
			}

			if( is_file($large) ){
				unlink($large);
			}

			$stmt = $this->pdo->prepare('delete from tbl_files where id = :id');
			$stmt->bindValue(':id', $f['id'], PDO::PARAM_INT);
            $stmt->execute();
        }
	}

	public function destroyAllAndFolder(File $file)
	{
		$this->destroyAll($file);

		$path = sprintf('%s/%s/%d', $file->getPath(), $file->getModule(), $file->getRelashionship());

		if( is_dir($path) ){
			rmdir($path);
		}
	}
}