<?php

namespace DataAccess;

use PDO;
use DataAccess\Entity\Contato;

class Contatos
{
	private $pdo;

	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function save(Contato $contato)
	{
		$stmt = $this->pdo->prepare('insert into tbl_contato (nome, email, telefone, assunto, mensagem, ip, created_at) values (:nome, :email, :telefone, :assunto, :mensagem, :ip, :created_at)');
		$stmt->bindValue(':nome', $contato->getNome(), PDO::PARAM_STR);
		$stmt->bindValue(':email', $contato->getEmail(), PDO::PARAM_STR);
		$stmt->bindValue(':telefone', $contato->getTelefone(), PDO::PARAM_STR);
		$stmt->bindValue(':assunto', $contato->getAssunto(), PDO::PARAM_STR);
		$stmt->bindValue(':mensagem', $contato->getMensagem(), PDO::PARAM_STR);
		$stmt->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
		$stmt->bindValue(':created_at', date('Y-m-d H:i:s'), PDO::PARAM_STR);
		$stmt->execute();
	}
}