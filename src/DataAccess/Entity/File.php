<?php

namespace DataAccess\Entity;

class File
{
	public $id;
	public $path;
	public $relashionship;
	public $filename;
	public $title;
	public $type;
	public $module;

	public function __construct($path = '')
	{
		$this->path = $path;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setPath($path)
	{
		$this->path = $path;
	}

	public function getPath()
	{
		return $this->path;
	}

	public function setRelashionship($relashionship)
	{
		$this->relashionship = $relashionship;
	}

	public function getRelashionship()
	{
		return $this->relashionship;
	}

	public function setFilename($filename)
	{
		$this->filename = $filename;
	}

	public function getFilename()
	{
		return $this->filename;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setType($type)
	{
		$this->type = $type;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setModule($module)
	{
		$this->module = $module;
	}

	public function getModule()
	{
		return $this->module;
	}

}