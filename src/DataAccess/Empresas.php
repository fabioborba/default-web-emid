<?php

namespace DataAccess;

use PDO;

class Empresas
{
	private $pdo;

	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function all()
	{
		$stmt = $this->pdo->prepare('select * from tbl_empresas where ativo = 1 order by empresa');
		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $data;
	}

	public function allByCity($city)
	{
		$stmt = $this->pdo->prepare('select * from tbl_empresas where ativo = 1 and cidade = :cidade');
		$stmt->bindValue(':cidade', $city, PDO::PARAM_INT);
		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $data;
	}

	public function find($id)
	{
		$stmt = $this->pdo->prepare('select * from tbl_empresas where ativo = 1 and id = :id');
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		return $data;
	}

	public function findTotalByEmail($email)
	{
		$stmt = $this->pdo->prepare('select count(*) from tbl_empresas where email = :email');
		$stmt->bindValue(':email', $email, PDO::PARAM_STR);
		$stmt->execute();

		$total = $stmt->fetchColumn();

		return $total;
	}

	public function findByEmail($email)
	{
		$stmt = $this->pdo->prepare('select * from tbl_empresas where email = :email');
		$stmt->bindValue(':email', $email, PDO::PARAM_STR);
		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		return $data;
	}

	public function findByLogin($email, $senha)
	{
		$stmt = $this->pdo->prepare('select * from tbl_empresas where ativo = 1 and email = :email and senha = md5(:senha)');
		$stmt->bindValue(':email', $email, PDO::PARAM_STR);
		$stmt->bindValue(':senha', $senha, PDO::PARAM_STR);
		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		return $data;
	}
}