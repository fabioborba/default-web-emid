<?php

namespace Session;

class Session
{
	public function __construct()
	{
		$uniqid = md5(uniqid(rand(), true));

		if( ! isset($_COOKIE['uniqid']) ){
			setcookie('uniqid', $uniqid);
		} else {
			$uniqid = $_COOKIE['uniqid'];
		}

		session_name($uniqid);
		session_start();
	}

	public function set($key, $value = null)
	{
		$_SESSION[$key] = $value;
	}

	public function get($key)
	{
		if( ! isset($_SESSION[$key]) ){
			return false;
		}

		return $_SESSION[$key];
	}

	public function destroy($key)
	{
		if( isset($_SESSION[$key]) ){
			unset($_SESSION[$key]);
		}
	}

	public function setFlashMessage($key, $value = null)
	{
		$_SESSION['flash_messages'][$key] = $value;
	}

	public function getFlashMessage($key)
	{
		if( ! isset($_SESSION['flash_messages'][$key]) )
		{
			return null;
		}

		$value = $_SESSION['flash_messages'][$key];
		unset($_SESSION['flash_messages'][$key]);

		return $value;
	}
}