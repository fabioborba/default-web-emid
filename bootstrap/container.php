<?php

$_container = new Pimple\Pimple();

$_container['session'] = $_container->share(function ($c) {
    return new Session\Session();
});

$_container['pdo'] = function ($c) {
    return new PDO(
		'mysql:host=localhost;dbname=NOMEDOBANCO',
		'USER',
		'SENHA',
		array(
			PDO::ATTR_PERSISTENT => true,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"
		)
	);
};

$_container['mail'] = function ($c) {
    $transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587)
    	->setUsername('autentica@email.com')
        ->setPassword('IegiLhNNpNdJKmX2XIHcOA');

    return Swift_Mailer::newInstance($transport);
};

$_container['path_files'] = $_path['path_files'];

$_container['files'] = function ($c) {
    return new DataAccess\Files($c['pdo']);
};

$_container['file'] = function ($c) {
    return new DataAccess\Entity\File($c['path_files']);
};

// pega dados para enviar para o banco

$_container['contatos'] = function ($c) {
    return new DataAccess\Contatos($c['pdo']);
};
// classe que tem atributos
$_container['contato'] = function ($c) {
    return new DataAccess\Entity\Contato();
};

