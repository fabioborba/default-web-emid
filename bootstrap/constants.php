<?php

	$_path = array(
		'get_url'  => "http://" . $_SERVER['SERVER_NAME'] . $_SERVER ['REQUEST_URI'],
		'url_site' => 'http://site.com.br', // Seo
		'includes' => 'includes/',
		'url_base'  	=> 'http://' . $_SERVER['HTTP_HOST'],
		'url_files' 	=> 'http://' . $_SERVER['HTTP_HOST'] . '/files',
		'path_files' 	=> __DIR__ . '/../files',
	);

	/* Link redes sociais */
	$_socialize = array(
		'whatsapp' => '46 9923-3932',
		'facebook' => 'https://www.facebook.com/empresa',
		'youtube' => 'https://www.youtube.com/'
	);

	$_config = array(
		'title' => 'Company name',
		'company' => 'Company name',
		'slogan' => '',
		'og_image' => '',
		'mail' => 'email@email',
		'phone' => array(
			'(46) 0000-0000',
			'(46) 9999-9999'
		),
		'address' => array(
			'street' => 'Avenida centro, numero - endereco',
			'zip' => '85760-000',
			'city' => 'Ampere - PR',
			'map' => 'embed.map'
		),
		'author' => array(
			'name' => 'E-mid',
			'url' => 'http://www.emid.com.br/',
			'start_year' => '2000',
			'current_year' => '2016',
			'copyright' => 'Copyright © 2016'
		)
	);


	$_route = array(
		'home' 				=> 'index.php',
		// suas rotas aqui 
		'contato' 			=> 'contato.php'
	);

	