<?php

	ini_set('display_errors', true);
	error_reporting(-1);

	date_default_timezone_set('America/Sao_Paulo');

	header('Content-type: text/html; charset=UTF-8');

	/* Include path */
	set_include_path(implode(PATH_SEPARATOR, array(
	    __DIR__ . '/../src',
	    get_include_path()
	)));

	/* PEAR autoloader */
	spl_autoload_register(function ($className)
	{
	    $filename = strtr($className, '\\', DIRECTORY_SEPARATOR) . '.php';
	    foreach (explode(PATH_SEPARATOR, get_include_path()) as $path) {
	        $path .= DIRECTORY_SEPARATOR . $filename;
	        if (is_file($path)) {
	            require_once $path;
	            return true;
	        }
	    }
	    return false;
	});

	require_once __DIR__ . '/constants.php';
	require_once __DIR__ . '/container.php';
	require_once __DIR__ . '/functions.php';
	require_once __DIR__ . '/../swiftmailer/lib/swift_required.php';