<?php

function redirect($url)
{
	header(sprintf('Location: %s', $url));
	exit;
}

function convert_real_to_decimal($value)
{
	return number_format(preg_filter('/\D/', '', $value) / 100, 2, '.', '');
}

function re_array_files(&$file_post) 
{
    $file_ary   = array();
    $file_count = count($file_post['name']);
    $file_keys  = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function get_slug_filename($filename)
{
	$slugifier = new Slug\Slugifier();

	$name 		= pathinfo($filename, PATHINFO_FILENAME);
	$extension 	= pathinfo($filename, PATHINFO_EXTENSION);
	
	return strtolower(sprintf('%s.%s', $slugifier->slugify($name), $extension));
}

function limitar_texto($texto, $limite, $break = true)
{
	if( mb_strlen($texto) <= $limite ){
		return $texto;
	}

	$break_word = ($break) ? strrpos(substr($texto, 0, $limite), ' ') : $limite;

    return substr($texto, 0, $break_word) . '...';
}

function factory_first_image($module, $id)
{
	global $_path, $_container;

	$image = $_container['files']->first('photos', $module, $id);

	if( ! $image ){
		return sprintf('%s/img/img_not_found.gif', rtrim($_path['url_base'], '/'));
	}

	return sprintf('%s/%s/%d/thumb_%s', rtrim($_path['url_files'], '/'), $module, $id, $image['filename']);
}

function factory_images($module, $id)
{
	global $_path, $_container;

	$images = $_container['files']->all('photos', $module, $id);
	$images_iterator = array();

	foreach ($images as $image) {
		$images_iterator[] = array_merge(
			$image,
			array('image_url' => sprintf('%s/%s/%d/thumb_%s', rtrim($_path['url_files'], '/'), $module, $id, $image['filename']))
		);
	}

	return $images_iterator;
}
 function getHeadersIndex() {
        echo '<title>' . $this->_title . "</title>\n";
        echo '<link href="' . SITE_PATH . '/lib/css/core.css" rel="stylesheet">' . "\n";
        echo '<link href="' . SITE_PATH . '/lib/css/bootstrap.min.css" rel="stylesheet">' . "\n";
        echo '<script src="' . SITE_PATH . '/lib/js/jquery.js"></script>' . "\n";
        echo '<script src="' . SITE_PATH . '/lib/js/bootstrap.min.js"></script>' . "\n";
        echo '<script src="' . SITE_PATH . '/lib/js/misc.js"></script>' . "\n";
    }
