


<section class="theme-red">
    <form action="form.php" class="formee form-orcamento validate">

        <h2 class="title upp">SOLICITE UM ORÇAMENTO</h2>
        
        <fieldset class="row">
            
            <div class="field col s12 m12 l6">
                <input type="text" name="name" placeholder="Nome:" required title="Insira seu nome">
            </div>
            <div class="field col s12 m12 l6">
                <input type="text" name="empresa" placeholder="Empresa:" required title="Nome da empresa">
            </div>
            <div class="field col s12 m6 l6">
                <input type="tel" id="phone" name="phone" placeholder="(xx) XXXX-XXXX" required title="Seu telefone">
            </div>

            <div class="field col s12 m6 l6">
                <input type="text" id="email" name="email" placeholder="seuemail@email.com" required title="Insira seu e-mail">
            </div>

            <div class="field col s12">
                <textarea name="message" id="" rows="3" placeholder="Insira sua mensagem:"></textarea>
            </div>

            <div class="field col s12 l6 offset-l6">
                <input type="submit" class="btn-default btn-full" value="ENVIAR">
            </div>

            <input type="hidden" name="action" value="orcamento">

        </fieldset>

        <div class="formee-msg alert">
            <p class="message"><strong>Por favor,</strong> preencha todos os campos!</p>
            <button class="closed">×</button>
        </div>

    </form>

</section>