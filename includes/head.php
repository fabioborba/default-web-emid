
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php echo isset($page['title']) ? $page['title'] . ' | ' . $_config['title'] : $_config['title']; ?></title>
<meta name="description" content="descricao">
<meta name="keywords" content="tags"> 
<meta name="author" content="E-MID Agência digital">

<meta property="og:locale" content="pt_BR">
<meta property="og:site_name" content="<?php echo $_config['company']; ?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo $_path['get_url']; ?>">
<meta name="og:title" content="<?php echo isset($page['title']) ? $page['title'] . ' | ' . $_config['title'] : $_config['title']; ?>">
<meta property="og:image" content="<?php echo isset($page['og_image']) ? $page['og_image'] : $_config['og_image']; ?>">

<link rel="stylesheet" href="/css/style.min.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500,300' rel='stylesheet' type='text/css'>



<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "<?php echo $_path['url_site']; ?>",
      "logo": "<?php echo $_path['url_site']; ?>/img/logo.png"
    }
</script>

