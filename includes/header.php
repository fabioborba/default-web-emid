<header class="header">
    
    <div class="wrapper">
        <a class="site-title" href="/">
            <img src="img/logo.png" alt="<?php echo $_config['company']; ?>">
        </a>

        <nav class="app-bar-nav">
            <input type="checkbox" id="control-nav" />
            <label for="control-nav" class="control-nav"></label>
            <label for="control-nav" class="control-nav-close"></label>
            
            <ul class="content-nav">
                <li><a href="<?php echo $_route['home']; ?>" class="item-nav">Início</a></li>
                <li><a href="<?php echo $_route['contato']; ?>" class="item-nav">Contato</a></li>
            </ul>
        </nav>
    </div>

</header>
