
<section class="breadcrumbs">
	
	<ul class="breadcrumb-widget">
		<li class="breadcrumb-label"><p>Você está em:</p></li>
		<li class="breadcrumb-home"><a href="/">Início</a></li>
		<?php
			foreach ($breadcrumb as $key => $value) :
				if ( is_string($key) ){
					$text = sprintf('<a href="%s">%s</a>', $value, $key);
				} else {
					$text = sprintf('<span>%s</span>', $value);
				}

				printf('<li class="breadcrumb-item">%s</li>', $text);

			endforeach;
		?>

	</ul>

</section>