<?php
	
	require_once 'bootstrap/bootstrap.php';
	$page = array(
		'title' => 'Titulo pagina'
	);

	/* Breadcrumb */
	$breadcrumb = array(
		$page['title']
	);

?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<?php include $_path['includes'] . 'head.php'; ?>
</head>
<body>

	<?php include $_path['includes'] . 'header.php'; ?>

	<main class="main wrapper">
	
		<header class="headline-title">
			<?php include $_path['includes'] . 'breadcrumb.php'; ?>
			<h1 class="title"><?php echo $page['title']; ?></h1>
		</header><!-- .headline-title -->


		<section class="main-container">
			
			<div class="main-content">
				
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero accusamus unde sequi excepturi temporibus distinctio incidunt ipsum facilis nihil id, hic quibusdam, explicabo at magnam, optio corporis quod omnis. Provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto sint nihil magnam nulla accusamus numquam iste repellat odit ex id, at corporis, ipsa, laboriosam magni? Fugiat soluta ex ab eaque.<br><br>

					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci mollitia numquam voluptas deserunt aliquam minima odio beatae, voluptatum laborum inventore autem est nobis qui, rem exercitationem explicabo, dignissimos ducimus sed.
				</p>

			</div><!-- .main-content -->

			<aside class="main-sidebar">
				<?php include $_path['includes'] . 'sidebar.php'; ?>
			</aside>

		</section><!-- .main-container -->

	</main><!-- .main -->

	<?php include $_path['includes'] . 'footer.php'; ?>

	
</body>
</html>